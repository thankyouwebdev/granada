<?php
	$php=phpversion();
	$php=substr($php,0,3);
	if ($php >= "5.3") {
		$fecha = date_create();
		$segundos = date_timestamp_get($fecha);
	}
	else {
		$fecha = new DateTime();
		$segundos = $fecha->format("U");
	}
	$expire = $segundos-2592000;
	setcookie("usuario", "", $expire);
	setcookie("hash_token", "", $expire);
	header('Location: index.php');
?>