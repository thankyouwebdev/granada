<?php
require_once("recursos/estatico/librerias/extractor-lru.php");
require_once("recursos/estatico/librerias/Encoding.php");
require_once('recursos/estatico/librerias/conectorpump.php');
require_once('sistema/configuracion.php');
#Se comprueba que existe petición de alguna url
if($_REQUEST['url']) {
	if(!filter_var($_REQUEST['url'], FILTER_VALIDATE_URL)) {
		echo "URL no valida, regrese a la pagina anterior.";
		exit;
	}
	if( isset($_SERVER['HTTPS'] ) ) {
		$https = "s";
	}
	else {
		$https = "";
	}
	#Codificamos la solicitud "url" para no tener problemas si es que ya tiene ampersands incluidos
	if(substr_count("http".$https.'://'.$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI], "&") > 0) {
		$enc_url = urlencode(str_replace('http'.$https.'://'.$_SERVER[HTTP_HOST].$_SERVER['PHP_SELF'].'?url=', "", "http".$https.'://'.$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI]));
		header('Location: '.$_SERVER['PHP_SELF'].'?url='.$enc_url);
	}
	else {
		#Se comprueba que existe alguna sesión activa
		if($_COOKIE["usuario"] && $_COOKIE["hash_token"]) {
			$usuario = $_COOKIE["usuario"];
			$hash_token = $_COOKIE["hash_token"];
			$query = mysql_query("SELECT token FROM token WHERE user='$usuario'", $con);
			$comp = mysql_fetch_array($query);
			$hash_token_r = hash("SHA256", $comp['token']);
			#Se comprueban las credenciales de las cookies, se previene suplantación de identidad
			if($hash_token == $hash_token_r) {
				$usuario = $_COOKIE["usuario"];
				$host = ConectorPump::extractorPumpid($usuario, "nodo");
				$query = "SELECT oauth.consumer_key, oauth.consumer_secret, oauth.conexion, token.token, token.token_secret ";
				$query.= "FROM oauth, token WHERE servidor='$host' and user='$usuario'";
				$query = mysql_query($query, $con);
				$cred = mysql_fetch_array($query);
				$conexion = $cred["conexion"];
				$oauth_token = $cred["token"];
				$oauth_token_secret = $cred["token_secret"];
				$oauth_consumer_key = $cred["consumer_key"];
				$oauth_consumer_secret = $cred["consumer_secret"];
				$datosu = ConectorPump::obtenerDatosusuario($usuario, $conexion, $oauth_consumer_key, $oauth_consumer_secret, $oauth_token, $oauth_token_secret, 'profile', '', $cliente);
				$seguidores = ConectorPump::obtenerDatosusuario($usuario, $conexion, $oauth_consumer_key, $oauth_consumer_secret, $oauth_token, $oauth_token_secret, 'followers', $datosu[0]['followers']['totalItems'], $cliente);
				$listas = ConectorPump::obtenerDatosusuario($usuario, $conexion, $oauth_consumer_key, $oauth_consumer_secret, $oauth_token, $oauth_token_secret, 'lists', '', $cliente);
				$imagen = $datosu[0][image][url];
				$nombre = $datosu[0][displayName];
				$url = $datosu[0][url];
				#Comprobamos opciones guardadas por el usuarios.
				if(isset($_COOKIE['opciones'])) {
					$opciones = explode(';;', $_COOKIE['opciones']);
					$checkg = "checked";
					$op =1;
				}
				else {
					$op = 0;
				}
				for($x=0;$x < $datosu[0]['followers']['totalItems']; $x++) {
					if(array_search($seguidores[0][items][$x][id], $opciones) !== false && $op) {
						$check = 'checked';
					}
					else {
						$check = '';
					}
					$seguidor.="<input type='checkbox' name='seguidor[]' id='seguidor".$x."' value='".$seguidores[0][items][$x][id]."'".$check."><label for='seguidor".$x."'>".$seguidores[0][items][$x][displayName]."</label>";
				}
				if(array_search('http://activityschema.org/collection/public', $opciones) !== false && $op) {
					$checkp = "checked";
				}
				if(array_search($seguidores[0]['url'], $opciones) !== false && $op) {
					$checks = "checked";
				}
				if(!$op) {
					$checks = "checked";
					$checkp = "checked";
				}
				$menu = "<img id='avatar' src='".$imagen."' alt='Avatar'><a href='".$url."'>".$nombre."</a><a href='salir.php'>[x]</a>";
				libxml_use_internal_errors(TRUE);
				include('recursos/estatico/esquema/cabecera.html');
				#Selector de Imágenes
				include('recursos/estatico/esquema/top_img.html');
				$url=$_REQUEST['url'];
				$datos = Extractor::extraer($url);
				function codificacion($string) {
					$codificacion = mb_detect_encoding($string, 'UTF-8, ISO-8859-1', true);
					if($codificacion == "UTF-8") {
						#$string = iconv("UTF-8","UTF-8//IGNORE",$string);
						$string = Encoding::fixUTF8($string);
						return $string;
						#echo "Debbuger(No estará en tu publicación esta linea, ignorar): Codificación Iso";
					}
					else {
						$string = Encoding::toUTF8($string);
						return $string;
						#echo "Debbuger(No estará en tu publicación esta linea, ignorar): Codificación utf-8";
					}
				}
				$titulo = htmlspecialchars($datos["titulo"], ENT_QUOTES);
				$descripcion = $datos["descripcion"];
				if($datos['charset'] != 'utf-8') {
					$titulo = htmlspecialchars(codificacion($datos["titulo"]), ENT_QUOTES);
					$descripcion = codificacion($datos["descripcion"]);
				}
				$etiquetas = $datos["palabrasclave"];
				$imagen = $datos["imagen"];
				$tamanio = count($imagen);
				for ($x=0;$x<$tamanio; $x++) {
					if($x==0) {
						$check = "checked";
					}
					else {
						$check = "";
					}
					include('recursos/estatico/esquema/url_imagen.html');
				}
				for ($x=0;$x<$listas[0][totalItems]; $x++) {
					if(array_search($listas[0][items][$x][id], $opciones) !== false && $op) {
						$check = 'checked';
					}
					else {
						$check = '';
					}
					$s_listas.="<input type='checkbox' name='coleccion[]' id='coleccion".($x+2)."' value='".$listas[0][items][$x][id]."'".$check."><label for='coleccion".($x+2)."'>".$listas[0][items][$x][displayName]."</label>";
				}
				echo '<input type="radio" name="radios" id="radio_a" value="0">';
				echo '<label for="radio_a">Sin Imagen</label></div>';
				#Editor derecho
				include('recursos/estatico/esquema/editor.html');
				include('recursos/estatico/esquema/foo_img.html');
				#pie de pagina
				include('recursos/estatico/esquema/pie.html');
			}
		}
		else {
			echo "Debe iniciar sesión";
			header('Location: index.php');
		}
	}
}
else {
	header('Location: index.php');
}
?>