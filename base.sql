-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `oauth`;
CREATE TABLE `oauth` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `servidor` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `consumer_key` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `consumer_secret` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `conexion` tinytext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `token` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `token_secret` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `verificador` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `acceso` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- 2014-08-16 01:32:57
