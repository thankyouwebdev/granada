<?php
class Universal {
	public static $userAgent = 'Granada Bot - http://granada.redaustral.tk/acerca.php';
	function cURL($url) {
		$ch = curl_init($url);  
		$opciones = array(
			CURLOPT_SSL_VERIFYPEER => false,
   		CURLOPT_RETURNTRANSFER => true,
   		CURLOPT_USERAGENT => self::$userAgent,
   		CURLINFO_HEADER_OUT => true,
   		CURLOPT_FOLLOWLOCATION => false,
   		CURLOPT_CONNECTTIMEOUT => 10,
   		CURLOPT_HTTPHEADER => array(
   		)
		);
		curl_setopt_array($ch, $opciones);
		$exec = curl_exec($ch);
		$info = curl_getinfo($ch);
		if(!$exec) {
			$info['error'] = curl_error($ch);
			$info['errno'] = curl_errno($ch);
		}
		curl_close($ch);
		$extra_info = explode(';',$info['content_type']);
		$charset = array_values(preg_grep("/charset.*/", $extra_info));
		$mime = array_values(preg_grep("/^.*\/.*$/", $extra_info));
		$mime = trim($mime[0]);
		$charset = explode('=', trim($charset[0]));
		$charset = $charset[1];
		return array($exec, $info, 'charset' => strtolower($charset), 'mime' => strtolower($mime));
	}
	function getLocation($curl) {
			$yy = $curl;
			$w = explode("\n",$yy);
			$TheShortURL = array_values(preg_grep('/'.str_replace('*', '.*', 'Location' ).'/', $w));
			$url2 = $TheShortURL[0];
			$url2 = str_replace("Location:", "", $url2);
			$url2 = trim($url2, " \t\n\r\0\x0B");
			$url2 = explode(' ', $url2);
			$url2 = array_values(preg_grep("/^http/", $url2));
			$url2 = $url2[0];
			if($url2) {
				return $url2;
			}
			else {
				return 0;
			}
	}
	function resolveShorturl($url) {	
		$curl = self::cURL($url);
		if($curl[1][http_code] == 200) {
			$url2 = self::getLocation($curl[0]);
			if($url2) {
				$respuesta = $url2;
			}
			else {
				$respuesta = $url;
			}
			return array($respuesta, 'cURL' => $curl[1]);
		}
		elseif($curl[1][http_code] == 301) {
			$curl2 = self::cURL($curl[1][redirect_url]);
			if($curl2[1][http_code] == 200) {
				$url2 = self::getLocation($curl2[0]);
				if($url2) {
					$respuesta = $url2;
				}
				else {
					$respuesta = $curl[1][redirect_url];
				}
				return array($respuesta, 'cURL' => $curl[1]);
			}
		}
		else {
			return array(0, 'cURL' => $curl[1]);
		}
	}
	function getDOMobject($url) {
		$host = parse_url($url,PHP_URL_HOST);
		$scheme = parse_url($url,PHP_URL_SCHEME);
		$curl = self::cURL($url);
		if($curl[1][http_code] == 200) {
			$dom = new domDocument;
			$dom->loadHTML($curl[0]);
			$dom->preserveWhiteSpace = false;
			return array($dom,$curl[1],'charset' => $curl['charset'],'mime' => $curl['mime']);
		}
		else {
			return array(0,$curl[1]);
		}	
	}
	function getArrayImage($dom) {
		$images = $dom->getElementsByTagName('img');
		$images_array = array();
		foreach($images as $img){              
			$images_array[] = array(
				'src' => $img->getAttribute('src'),      
				'alt' => $img->getAttribute('alt'),
			);
		}
		return $images_array;
	}
	function completarURLimagen($images_array, $url) {
		$host = parse_url($url,PHP_URL_HOST);
		$scheme = parse_url($url,PHP_URL_SCHEME);
		$path = parse_url($url,PHP_URL_PATH);
		if(!count(preg_grep('/.*\/$/', array($path)))>0) {
			$partes = explode('/', $path);
			$partes = array_reverse($partes);
			$path = str_replace($partes[0], '', $path);
		}
		if(strlen($path)>0) {
			$path = trim($path, '/').'/';
		}
		$tamanio = count($images_array);
		$imagen = array();
		for ($x=0;$x<$tamanio; $x++) {
			if($images_array[$x]['src']) {
				$url_image = array($images_array[$x]['src']);
				if(!preg_grep("/^http/", $url_image) && !preg_grep("/^\//", $url_image)) {
					$camino = trim($images_array[$x]['src'], '/');
					$image = $scheme.'://'.$host.'/'.$path.$camino;
					array_unshift($imagen, $image);
				}
				elseif(preg_grep("/^\//", $url_image)) {
					$camino = trim($images_array[$x]['src'], '/');
					$image = $scheme.'://'.$host.'/'.$camino;
					array_unshift($imagen, $image);
				}
				else {
					$image = $images_array[$x]['src'];
					array_unshift($imagen, $image);
				}
			}
		}
		return array_reverse(array_unique($imagen));
	}
	function imagen($url, $dom) {
		if(!$dom) {
			$dom = self::getDOMobject($url);
			if($dom[0]) {
				$images_array = self::getArrayImage($dom[0]);
				$imagen = self::completarURLimagen($images_array, $url);
				return array($imagen, 'cURL' => $dom[1]);
			}
		}	
		else {
			$images_array = self::getArrayImage($dom);
			$imagen = self::completarURLimagen($images_array, $url);
			return $imagen;
		}
	}
	function getTitle($dom) {
		$list = $dom->getElementsByTagName('title');
		if ($list->length > 0) {
			$titulo = $list->item(0)->textContent;
    	}
    	return trim($titulo, " \t\n\r\0\x0B");
	}
	function titulo($url, $dom) {
		if(!$dom) {
			$dom = self::getDOMobject($url);
			if($dom[0]) {
				$titulo = self::getTitle($dom[0]);
				return array($titulo, $dom[1]);
			}
		}	
		else {
			$titulo = self::getTitle($dom);
			return $titulo;
		}
	}
	function getPalabrasClave($dom) {
		$meta = $dom->getElementsByTagName('meta');
		$metas = array();
		foreach($meta as $mt){              
			$metas[$mt->getAttribute('name')] = $mt->getAttribute('content');
		}
		if($metas['keywords']) {
			return implode(',',array_unique(explode(',',strtolower(str_replace(' ', '', trim($metas['keywords'], " \t\n\r\0\x0B"))))));
		}
		elseif($metas['KEYWORDS']) {
			return implode(',',array_unique(explode(',',strtolower(str_replace(' ', '', trim($metas['KEYWORDS'], " \t\n\r\0\x0B"))))));
		}
		else {
			return false;
		}
	}
	function palabrasclave($url, $dom) {
		if(!$dom) {
			$dom = self::getDOMobject($url);
			if($dom[0]) {
				$palabrasclave = self::getPalabrasClave($dom[0]);
				return array($palabrasclave, $dom[1]);
			}
		}
		else {
			$palabrasclave = self::getPalabrasClave($dom);
			return $palabrasclave;
		}
	}
	function getDescription($dom) {
		$meta = $dom->getElementsByTagName('meta');
		$metas = array();
		foreach($meta as $mt){              
			$metas[$mt->getAttribute('name')] = $mt->getAttribute('content');
		}
		if($metas['description']) {
			return trim($metas['description'], " \t\n\r\0\x0B");
		}
		elseif($metas['DESCRIPTION']) {
			return trim($metas['DESCRIPTION'], " \t\n\r\0\x0B");
		}
		else {
			return false;
		}
	}
	function descripcion($url, $dom) {
		if(!$dom) {
			$dom = self::getDOMobject($url);
			if($dom[0]) {
				$descripcion = self::getDescription($dom[0]);
				return array($descripcion, $dom[1]);
			}
		}
		else {
			$descripcion = self::getDescription($dom);
			return $descripcion;
		}
	}
	function extractor($url, $dom) {
		if(!$dom) {
			$dom = self::getDOMobject($url);
			if($dom[0]) {
				$imagen = Universal::imagen($url, $dom[0]);
				$titulo = Universal::titulo($url, $dom[0]);
				$descripcion = Universal::descripcion($url, $dom[0]);
				$palabrasclave = Universal::palabrasclave($url, $dom[0]);
				return array(
					'imagen' => $imagen,
					'titulo' => $titulo,
					'descripcion' => $descripcion,
					'palabrasclave' => $palabrasclave,
					'charset' => $dom['charset'],
					'mime' => $dom['mime'],
					'ddf' => array('cURL' => $dom[1])
				);
			}
		}
		else {
			$imagen = Universal::imagen($url, $dom);
			$titulo = Universal::titulo($url, $dom);
			$descripcion = Universal::descripcion($url, $dom);
			$palabrasclave = Universal::palabrasclave($url, $dom);
			return array(
				'imagen' => $imagen,
				'titulo' => $titulo,
				'descripcion' => $descripcion,
				'palabrasclave' => $palabrasclave
			);
		}
	}
}
class Jamendo {
	public static $idioma = array(
		'es' => array(
			'by' => 'por',
			'Play' => 'Reproducir',
			'Open in Jamendo' => 'Abrir en Jamendo',
			'Download' => 'Descarga',
			'Official Website' => 'Sitio Web Oficial',
			'License' => 'Licencia',
			'Album' => 'Álbum',
			'Playlist' => 'Lista de Reproducción'
		),
		'en' => array(
			'by' => 'by',
			'Play' => 'Play',
			'Open in Jamendo' => 'Open in Jamendo',
			'Download' => 'Download',
			'Official Website' => 'Official Website',
			'License' => 'License',
			'Album' => 'Album',
			'Playlist' => 'Playlist'
		)
	);
	function obtDatos($url_esquema, $peticion, $id, $para_ext) {
		$curl = Universal::cURL($url_esquema.'://api.jamendo.com/v3.0/'.$peticion.'/?'.$para_ext.'client_id=d6b4afd7&format=json&imagesize=600&id='.$id);
		if($curl[1][http_code] == 200) {
			return array($curl[0], 'cURL' => $curl[1], 'charset' => $curl['charset'], 'mime' => $curl['mime']);
		}
		else {
			return array(0, 'cURL' => $curl[1]);
		}
	}
	function album($url, $url_esquema, $url_camino, $lenguaje) {
		if(!$lenguaje) {
			$lenguaje = 'es';
		}
		$idi = self::$idioma[$lenguaje];
		$peticion = "albums";
		$album = preg_grep("/^a[0-9]{2,}$/", $url_camino);
		$album = array_values($album);
		$id = str_replace("a", "", $album[0]);
		#echo "Id del Album: ".$id;
		$para_ext = 'imagesize=600&';
		$obtDatos = Jamendo::obtDatos($url_esquema, $peticion, $id, $para_ext);
		if($obtDatos[0]) {
			$datos = json_decode($obtDatos[0],true);
			$image = $datos['results'][0]['image'];
			$titulo = htmlspecialchars($idi['Album'].' '.$datos['results'][0]['name'].' '.$idi['by'].' '.$datos['results'][0]['artist_name'].' - Jamendo');
			$descripcion = '['.$idi['Open in Jamendo'].']('.$datos['results'][0]['shareurl'].') - ['.$idi['Download'].'(mp3)]('.$datos['results'][0]['zip'].")\n\n";
			$desc = Universal::descripcion($url);
			$descripcion .= $desc[0];
			return array(
				'imagen' => array($image),
				'titulo' => $titulo,
				'descripcion' => $descripcion,
				'charset' => $obtDatos['charset'],
				'mime' => $obtDatos['mime'],
				'ddf' => array('obtDatos' => $obtDatos['cURL']));
		}
		else {
			return array(0, 'ddf' => array('obtDatos' => $obtDatos['cURL']));
		}
	}
	function playlist($url, $url_esquema, $url_camino, $lenguaje) {
		if(!$lenguaje) {
			$lenguaje = 'es';
		}
		$idi = self::$idioma[$lenguaje];
		$peticion = "playlists";
		$album = preg_grep("/^p[0-9]{2,}$/", $url_camino);
		$album = array_values($album);
		$id = str_replace("p", "", $album[0]);
		#echo "Id del Album: ".$id;
		$para_ext = 'imagesize=600&';
		$obtDatos = Jamendo::obtDatos($url_esquema, $peticion, $id, $para_ext);
		if($obtDatos[0]) {
			$datos = json_decode($obtDatos[0],true);
			$image = 'http://imgjam2.jamendo.com/avatars/questionmark/avatar.100.png';
			$titulo = htmlspecialchars($idi['Playlist'].' '.$datos['results'][0]['name'].' '.$idi['by'].' '.$datos['results'][0]['user_name'].' - Jamendo');
			$descripcion = '['.$idi['Open in Jamendo'].']('.$datos['results'][0]['shareurl'].') - ['.$idi['Download'].'(mp3)]('.$datos['results'][0]['zip'].")\n\n";
			$desc = Universal::descripcion($url);
			$descripcion .= $desc[0];
			return array(
				'imagen' => array($image),
				'titulo' => $titulo,
				'descripcion' => $descripcion,
				'charset' => $obtDatos['charset'],
				'mime' => $obtDatos['mime'],
				'ddf' => array('obtDatos' => $obtDatos['cURL']));
		}
		else {
			return array(0, 'ddf' => array('obtDatos' => $obtDatos['cURL']));
		}
	}
	function artista($url, $url_esquema, $url_camino, $lenguaje) {
		if(!$lenguaje) {
			$lenguaje = 'es';
		}
		$idi = self::$idioma[$lenguaje];
		$peticion = "artists";
		$artista = preg_grep("/^[0-9]{1,}$/", $url_camino);
		$artista = array_values($artista);
		$id = $artista[0];
		#echo "Id del artista: ".$id;
		$para_ext = "";
		$obtDatos = Jamendo::obtDatos($url_esquema, $peticion, $id, $para_ext);
		if($obtDatos[0]) {
			$datos = json_decode($obtDatos[0],true);
			$image = $datos['results'][0]['image'];
			$titulo = htmlspecialchars($datos['results'][0]['name'].' - Jamendo');
			$descripcion = '['.$idi['Official Website'].']('.$datos['results'][0]['website'].")\n\n";
			$desc = Universal::descripcion($url);
			$descripcion .= $desc[0];
			return array(
				'imagen' => array($image),
				'titulo' => $titulo,
				'descripcion' => $descripcion,
				'charset' => $obtDatos['charset'],
				'mime' => $obtDatos['mime'],
				'ddf' => array('obtDatos' => $obtDatos['cURL']));
		}
		else {
			return array(0, 'ddf' => array('obtDatos' => $obtDatos['cURL']));
		}
	}
	function pista($url, $url_esquema, $url_camino, $lenguaje) {
		if(!$lenguaje) {
			$lenguaje = 'es';
		}
		$idi = self::$idioma[$lenguaje];
		$peticion = "tracks";
		$track = preg_grep("/^[0-9]{1,}$/", $url_camino);
		$track = array_values($track);
		$id = $track[0];
		#echo "Id del Track: ".$id;
		$para_ext = 'imagesize=150&audioformat=ogg&audiodlformat=ogg&';
		$obtDatos = Jamendo::obtDatos($url_esquema, $peticion, $id, $para_ext);
		if($obtDatos[0]) {
			$datos = json_decode($obtDatos[0],true);
			$image = $datos['results'][0]['album_image'];
			$titulo = htmlspecialchars($datos['results'][0]['name'].' '.$idi['by'].' '.$datos['results'][0]['artist_name'].' - Jamendo');
			$descripcion = '['.$idi['Play'].']('.$datos['results'][0]['audio'].') - ['.$idi['Download'].'(ogg)]('.$datos['results'][0]['audiodownload'].') - ['.$idi['License'].']('.$datos['results'][0]['license_ccurl'].")\n\n";
			$desc = Universal::descripcion($url);
			$descripcion .= $desc[0];
			return array(
				'imagen' => array($image),
				'titulo' => $titulo,
				'descripcion' => $descripcion,
				'charset' => $obtDatos['charset'],
				'mime' => $obtDatos['mime'],
				'ddf' => array('obtDatos' => $obtDatos['cURL']));
		}
		else {
			return array(0, 'ddf' => array('obtDatos' => $obtDatos['cURL']));
		}
	}
	function extractor($url) {
		$url_esquema = parse_url($url,PHP_URL_SCHEME);
		$url_camino = explode("/",parse_url($url,PHP_URL_PATH));
		if(strpos($url, '/list/')) {
			if(preg_grep("/^a[0-9]{2,}$/", $url_camino)) {
				$respuesta = Jamendo::album($url, $url_esquema, $url_camino);
			}
			elseif(preg_grep("/^p[0-9]{2,}$/", $url_camino)) {
				$respuesta = Jamendo::playlist($url, $url_esquema, $url_camino);
			}
		}
		elseif(strpos($url, '/artist/')) {
			$respuesta = Jamendo::artista($url, $url_esquema, $url_camino);
		}
		elseif(strpos($url, '/track/')) {
			$respuesta = Jamendo::pista($url, $url_esquema, $url_camino);
		}
		else {
			$respuesta = Universal::extractor($url);
		}
		return $respuesta;
	}
}
class Vimeo {
	function extractor($url) {
		$codigo = parse_url($url,PHP_URL_PATH);
		$codigo = trim($codigo, "/");
		$url2 = 'http://vimeo.com/api/v2/video/'.$codigo.'.php';
		$curl = Universal::cURL($url2);
		if($curl[1][http_code] == 200) {
			$return = unserialize($curl[0]);
			return array(
				'imagen' => array($return[0]["thumbnail_large"]),
				'titulo' => $return[0]["title"],
				'descripcion' => strip_tags($return[0]["description"]),
				'charset' => $curl['charset'],
				'mime' => $curl['mime'],
				'ddf' => array('cURL Vimeo' => $curl[1])
			);
		}
		else {
			return array(0, 'ddf' => array('cURL Vimeo' => $curl[1]));
		}
	}
}
class Youtube {
	function obtCodigo($url) {
		$peticion = array($url);
		foreach (explode("&", parse_url($url,PHP_URL_QUERY)) as $part) {
			list($key, $value) = explode("=",$part);
			$pet = array($key => $value);
			$peticion = array_merge($peticion, $pet);
		}
		return $peticion["v"];
	}
	function extractor($url) {
		$codigo = Youtube::obtCodigo($url);
		$image = "https://img.youtube.com/vi/".$codigo."/0.jpg";
		$dom = Universal::getDOMobject($url);
		if($dom[0]) {
			$titulo = Universal::titulo($url, $dom[0]);
			$descripcion = Universal::descripcion($url, $dom[0]);
		}
		$ddf = array(
			'cURL' => $dom[1]
		);
		return array(
			'imagen' => array($image),
			'titulo' => $titulo,
			'descripcion' => $descripcion,
			'charset' => $dom['charset'],
			'mime' => $dom['mime'],
			'ddf' => $ddf
		);
	}
}
class Diaspora {
	function isDiaspora($dom) {
		$list = $dom->getElementsByTagName('script');
		if ($list->length > 0) {
			for($x=0;$x < $list->length; $x++) {
				$coincidencia = strpos(trim($list->item($x)->textContent, " \t\n\r\0\x0B"), "Diaspora.I18n.load");
				if($coincidencia !== false) {
					return true;
				}
			}
	    }
	    else {
	    	return false;
	    }
	}
	function extractor($url) {
		$url_esquema = parse_url($url,PHP_URL_SCHEME);
		$url_camino = explode("/",parse_url($url,PHP_URL_PATH));
		if(strpos($url, '/posts/')) {
				$curl = Universal::cURL($url.'.json');
				if($curl[1][http_code] == 200) {
					$user = json_decode($curl[0], true);
					if($user['public']) {
						$respuesta = array(
							'titulo' => $user['title'].' | Diaspora*',
							'descripcion' => '**'.$user['author']['name'].'** | *'.$user['author']['diaspora_id'].'* | '.$user['created_at']."\n\n".$user['text'],
							'imagen' => array($user['author']['avatar']['large']),
							'ddf' => array('cURL' =>$curl[1])
						);
						$etiquetas = array();
						if(preg_match_all('/#[a-zA-Z0-9áéíóúüñ\-_]{1,}/', $user['text'], $etiquetas)) {
							foreach($etiquetas[0] as $tag) {
								$respuesta['palabrasclave'][] = str_replace('#', '', $tag);
							}
							$respuesta['palabrasclave'] = implode(',', $respuesta['palabrasclave']);
						}
						if(preg_match_all('/(#[a-zA-Z0-9áéíóúüñ\-_]{1,} ){1,}#[a-zA-Z0-9áéíóúüñ\-_]{1,}/', $user['text'], $etiquetas)) {
							foreach($etiquetas[0] as $tag) {
								$respuesta['descripcion'] = str_replace($tag, '',$respuesta['descripcion']);
							}
						}
						if(preg_match_all('/#[a-zA-Z0-9áéíóúüñ\-_]{1,}/', $respuesta['descripcion'], $etiquetas)) {
							foreach($etiquetas[0] as $tag) {
								$respuesta['descripcion'] = str_replace($tag, str_replace('#', '', $tag), $respuesta['descripcion']);
							}
						}
						$respuesta['palabrasclave'] = strtolower($respuesta['palabrasclave']);
						if($user['open_graph_cache']['image']) {
							$respuesta['imagen'][] = $user['open_graph_cache']['image'];
						}
						if($user['o_embed_cache']['data']['thumbnail_url']) {
							$respuesta['imagen'][] = $user['o_embed_cache']['data']['thumbnail_url'];
						}
						if($user['photos']) {
							$count = count($user['photos']);
							for($x=0; $x < $count; $x++) {
								$respuesta['imagen'][] = $user['photos'][$x]['sizes']['large'];
							}
						}
						return $respuesta;
					}
					else {
					$respuesta = array(
						'titulo' => 'Diaspora*',
						'descripcion' => 'Este post no es publico',
						'imagen' => array(),
						'ddf' => array('cURL' =>$curl[1])
					);
					return $respuesta;
					}
				}
				else {
					return array(0, 'cURL' => $curl[1]);
				}
		}
		else {
			$respuesta = Universal::extractor($url);
		}
		return $respuesta;
	}
}
class Dailymotion {
	function video($url) {
		$codigo = explode("/",parse_url($url,PHP_URL_PATH));
		$codigo = $codigo[count($codigo)-1];
		$url = 'https://api.dailymotion.com/video/'.$codigo.'?fields=3d,id,description,duration_formatted,explicit,language,poster,poster_270x360_url,thumbnail_360_url,title,owner';
		$curl = Universal::cURL($url);
		if($curl[1][http_code] == 200) {
			$video = json_decode($curl[0], true);
			$descripcion = array();
			if($video['3d']) {
				array_unshift($descripcion, '**3D**');
			}
			if($video['explicit']) {
				array_unshift($descripcion, '**Contenido Explicito**');
			}
			array_unshift($descripcion, 'Duración: '.$video['duration_formatted']);
			if($video['language']) {
				array_unshift($descripcion, 'Lenguaje: '.$video['language']);
			}
			if($video['owner']) {
				array_unshift($descripcion, 'Por '.$video['owner']);
			}
			$descripcion = implode(' | ', $descripcion);
			$descripcion .= "\n\n".$video['description'];
			if($video['poster']) {
				$imagen = $video['poster_270x360_url'];
			}
			else {
				$imagen = $video['thumbnail_360_url'];
			}
			return array(
				'imagen' => array($imagen),
				'titulo' => $video['title'].' - Dailymotion',
				'descripcion' => $descripcion,
				'charset' => $curl['charset'],
				'mime' => $curl['mime'],
				'ddf' => array('cURL' => $curl[1]));
		}
		else {
			return array(0, 'ddf' => array('cURL' => $curl[1]));
		}
	}
	function extractor($url) {
		$url_esquema = parse_url($url,PHP_URL_SCHEME);
		$url_camino = explode("/",parse_url($url,PHP_URL_PATH));
		if(strpos($url, '/video/')) {
			$respuesta = Dailymotion::video($url);
		}
		else {
			$respuesta = Universal::extractor($url);
		}
		return $respuesta;
	}
}
class Extractor {
	function extraer($url) {
		$resolveShorturl = Universal::resolveShorturl($url);
		if($resolveShorturl[0]) {
			$url = $resolveShorturl[0];
			$host = parse_url($url,PHP_URL_HOST);
			$camino = parse_url($url,PHP_URL_PATH);
			if(($host=="www.youtube.com" && $camino=="/watch") || ($host=="youtube.com" && $camino=="/watch")) {
				$datos = Youtube::extractor($url);
			}
			elseif($host=="www.vimeo.com" || $host=="vimeo.com") {
				$datos = Vimeo::extractor($url);
			}
			elseif($host=="www.jamendo.com" || $host=="jamendo.com") {
				$datos = Jamendo::extractor($url);
			}
			elseif($host=="www.dailymotion.com" || $host=="dailymotion.com") {
				$datos = Dailymotion::extractor($url);
			}
			else {
				$dom = Universal::getDOMobject($url);
				if($dom[0]) {
					if(Diaspora::isDiaspora($dom[0])) {
						$datos = Diaspora::extractor($url);
					}
					else {
						$datos = Universal::extractor($url, $dom[0]);
					}
				}
				$datos['charset'] = $dom['charset'];
				$datos['mime'] = $dom['mime'];
			}
			$datos['ddf']['resolveShorturl'] = $resolveShorturl;
			return $datos;
		}
		else {
			return array(0, 'resolveShorturl' => $resolveShorturl);
		}
	}
}
?>