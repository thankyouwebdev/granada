<?php
include('recursos/estatico/librerias/Parsedown.php');
require_once('recursos/estatico/librerias/conectorpump.php');
require_once('sistema/configuracion.php');
$url = $_POST["url"];
$imagen = $_POST["radios"];
$usuario = $_POST["usuario"];
$titulo = stripslashes($_POST["titulo"]);
$descripcion = Parsedown::instance()->parse($_POST["descripcion"]);
$descripcion = str_replace('\\', '', $descripcion);
$etiquetas = explode(',', str_replace(' ', '', $_POST["etiquetas"]));
foreach($etiquetas as $tag) {
	$tags .= '#'.$tag.' ';
}
$host = ConectorPump::extractorPumpid($usuario, "nodo");
$nombre = ConectorPump::extractorPumpid($usuario, "alias");
$query = "SELECT oauth.consumer_key, oauth.consumer_secret, oauth.conexion, token.token, token.token_secret ";
$query.= "FROM oauth, token WHERE servidor='$host' and user='$usuario'";
$query = mysql_query($query, $con);
if(!$query) {
	echo "Hubo un error al solicitar información a la base de datos: <br>".mysql_errno()." - ".mysql_error();
	exit;
}
$cred = mysql_fetch_array($query);
$conexion = $cred["conexion"];
$oauth_token = $cred["token"];
$oauth_token_secret = $cred["token_secret"];
$oauth_consumer_key = $cred["consumer_key"];
$oauth_consumer_secret = $cred["consumer_secret"];
#Guardamos opciones del usuario
if($_POST['guardar']) {
	if(!$_POST["coleccion"]) {
		$array1 = array();
	}
	else {
		$array1 = $_POST["coleccion"];
	}
	if(!$_POST["seguidor"]) {
		$array2 = array();
	}
	else {
		$array2 = $_POST["seguidor"];
	}
	$array_opciones = array_merge($array1, $array2);
	$cadena = implode(';;', $array_opciones);
	$segundos = ConectorPump::selloTiempo();
	$expire = $segundos+2592000;
	$sc = setcookie("opciones", $cadena, $expire);
	if(!$sc) {
		echo "Necesita habilitar el uso de cookies en este sitio si desea guardar su selección de destinatarios";
	}
}
else {
	$segundos = ConectorPump::selloTiempo();
	$expire = $segundos-2592000;
	$sc = setcookie("opciones", '', $expire);
	if(!$sc) {
		echo "Necesita habilitar el uso de cookies en este sitio si desea guardar su selección de destinatarios";
	}
}
$cc = array();
$to = array();
for ($x=0;$x < count($_POST["coleccion"]); $x++) {
	$coleccion = $_POST["coleccion"][$x];
		$cccoleccion = true;
		$cc = array_merge($cc, array(array(
				'objectType' => 'collection',
				'id' => $coleccion
		)));
}
for ($x=0;$x < count($_POST["seguidor"]); $x++) {
	$seguidor = $_POST["seguidor"][$x];
		$toseguidor = true;
		$to = array_merge($to, array(array(
				'objectType' => 'person',
				'id' => $seguidor
		)));
}
if(!$imagen) {
			$content	= $descripcion.'<br>'.$tags.'<br>Fuente: <a href="'.$url.'" target="_blank">'.$url.'</a>';
}
else {
	$content = '<p align="center"><img src="'.$imagen.'" ></p>'.$descripcion.'<br>'.$tags.'<br>Fuente: <a href="'.$url.'" target="_blank">'.$url.'</a>';
}
$cc = array('cc' => $cc);
$to = array('to' => $to);
$object = array(
	'displayName' => $titulo,
	'content' => $content,
	'objectType' => 'note'
);
$posteo = array(
	'verb' => 'post',
	'object' => $object
);
if($cccoleccion) {
	$posteo = array_merge($posteo, $cc);
}
if($toseguidor) {
	$posteo = array_merge($posteo, $to);
}
#Enviamos la nota y si es pública se envía al ofirehose.
$nota = ConectorPump::enviarNota($posteo, $oauth_consumer_key, $oauth_consumer_secret, $oauth_token, $oauth_token_secret, $conexion, $usuario, $cliente);
if($nota[0]) {
	if(array_search('http://activityschema.org/collection/public', $_POST["coleccion"]) !== false) {
		$post = ConectorPump::pingFirehose('https://ofirehose.com/ping', $nota[0]);
	}
	$actividad = json_decode($nota[0], true);
	if($actividad['object']['displayName']) {
		$titulo = $actividad['object']['displayName'];
	} else {
		$titulo = "La Nota";
	}
	$url = $actividad['object']['url'];
	$mensaje = rawurlencode(base64_encode("<b><a href=".$url.">".$titulo."</a></b> ha sido escrita con éxito."));
	header('Location: index.php?mensaje='.$mensaje);
}
else {
	$mensaje = rawurlencode(base64_encode("Error al escribir el mensaje, intente nuevamente, si el problema persiste, contacte al administrador."));
	header('Location: index.php?mensaje='.$mensaje);
}
?>